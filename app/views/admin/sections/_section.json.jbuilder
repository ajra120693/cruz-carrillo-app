json.extract! section, :id, :description, :created_at, :updated_at
json.url admin_section_path(section, format: :json)
