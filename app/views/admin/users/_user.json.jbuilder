json.extract! user, :id, :name, :lastname, :dni, :birthdate, :address, :email, :created_at, :updated_at
json.url admin_users_path(user, format: :json)
