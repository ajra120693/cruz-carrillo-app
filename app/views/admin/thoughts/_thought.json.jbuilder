json.extract! thought, :id, :thought, :author, :created_at, :updated_at
json.url admin_thought_path(thought, format: :json)
