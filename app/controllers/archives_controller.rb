class ArchivesController < Admin::AdminController
  # load_and_authorize_resource
  before_action :authenticate_user!
  before_action :require_user, only: [:new, :create, :destroy]

  def index
    @archives = Archive.all
  end

  def new
    @archive = Archive.new
  end

  def create
    @archive = Archive.new(archive_params)
    @archive.user = current_user

    if @archive.save
      flash[:success] = "El archivo titulado '#{@archive.name}' ha sido publicado exitosamente."
      redirect_to archives_path
    else
      flash.now[:danger] = "El archivo no se ha podido publicar. Por favor corrija los siguientes errores"
      render "new"
    end
  end

  def destroy
    @archive = Archive.find(params[:id])
    @archive.destroy
    flash[:info] = "El archivo titulado '#{@archive.name}' ha sido eliminado exitosamente."
    redirect_to archives_path
  end

private

  def archive_params
    params.require(:archive).permit(:name, :description, :attachment, :section_id)
  end
end
