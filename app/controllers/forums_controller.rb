class ForumsController < Admin::AdminController
  before_action :authenticate_user!
  before_action :set_forum, only: [:show, :edit, :update, :destroy]
  before_action :require_show_user, only: [:show]
  before_action :require_user, only: [:new, :create, :edit, :update, :destroy]


  def index
    @forums = Forum.all
  end
  
  def new    
    @forum = Forum.new
  end

  def create
    @forum = Forum.new(forum_params)
    @forum.user = current_user
    if @forum.save
      flash[:success] = "Felicidades, su publicación ha sido creada exitosamente."
      redirect_to @forum
    else
      flash[:danger] = "Lamentablemente el contenido no se ha podido publicar. Por favor corrija los siguientes errores."
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @forum.update(forum_params)
      flash[:success] = "Felicidades, su contenido ha sido modificado exitosamente."
      redirect_to @forum
    else
      flash[:danger] = "Lamentablemente el contenido no se ha podido publicar. Por favor corrija los siguientes errores."
      render 'edit'
    end
  end

  def destroy  
    @forum.destroy
    flash[:info] = "Su contenido titulado '#{@forum.title}' ha sido borrado exitosamente."
    redirect_to forums_path
  end

private

  def forum_params
    params.require(:forum).permit(:comment, :title, :section_id)
    # , :section_id
  end

  def set_forum
    @forum = Forum.find(params[:id])
  end

  def require_show_user
    unless ((current_user == @forum.user) || (current_user.sections.take!.id == @forum.section_id && (cannot? :create, Forum)))
      flash[:danger] = "You must be logged in to perform that action"
      redirect_to root_path
    end
  end
end
