class HomeController < PublicController
  
  @id_tought = 0

  def index
  	#@news = New.pluck(:id, :name, :date_init, :date_end, :created_at, :updated_at)
  	@news =	New.select("id as id, name as title, description as description, date_init as start, date_end + 1  as end")

  	@users = User.where(id: UserRole.where(role_id: Role.where(name: ['Administrativo', 'Profesor', 'Superadmin']).pluck(:id)).pluck(:user_id))
  	@user_roles = UserRole.where(role_id: Role.where(name: ['Administrativo', 'Profesor', 'Superadmin']))
  	@roles = Role.where(name: ['Administrativo', 'Profesor', 'Superadmin'])

  	#Recordar despues colocar Status Activo
  	count = Thought.count
  	random_offset = rand(count)
  	@thought = Thought.offset(random_offset).first
    @id_thought = @thought.id
  end


  def return_tought
    @thought = Thought.limit(1).order("RANDOM()")
    while @thought.ids == @id_thought do
      @thought = Thought.limit(1).order("RANDOM()")
    end
    @id_thought = @thought.ids
    render :json => (@thought)
  end

end
