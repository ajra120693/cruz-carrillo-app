class ApplicationController < ActionController::Base
	layout 'public'
	
	before_action :configure_permitted_parameters, if: :devise_controller?

	rescue_from CanCan::AccessDenied do |exception|
		flash[:danger] = "No estás autorizado para acceder a esa página."
    	redirect_to main_app.root_url
	end

	rescue_from(ActionController::RoutingError) {
	    flash[:danger] = "No se ha localizado la página."
    	redirect_to main_app.root_url
	}

	rescue_from(ActionController::UnknownFormat){
		flash[:danger] = "No se ha localizado la página."
		puts 'Error: No hay Vistas para ese controllador/modelo'
    	redirect_to main_app.root_url	
	}

	def error_redirect
		flash[:danger] = "No se ha localizado la página."
    	redirect_to main_app.root_url
	end

	protected
	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute, :name, :lastname, :dni, :birthdate, :address])
		devise_parameter_sanitizer.permit(:account_update, keys: [:attribute, :name, :lastname, :dni, :birthdate, :address])
	end

	def require_user
    if cannot? :create, Forum
      flash[:danger] = "No estás autorizado para acceder a esa página."
      redirect_to root_path
    end
	end
end
