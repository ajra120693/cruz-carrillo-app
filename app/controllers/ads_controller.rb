class AdsController < Admin::AdminController
  before_action :authenticate_user!
  before_action :set_ad, only: [:show, :edit, :update, :destroy]
  before_action :require_show_user, only: [:show]
  before_action :require_user, only: [:new, :create, :edit, :update, :destroy]
  
  def index
    @ads = Ad.all
  end

  def new
    @ad = Ad.new
  end

  def create
    @ad = Ad.new(ad_params)
    @ad.user = current_user
    if @ad.save
      flash[:success] = "Felicidades, su anuncio ha sido creada exitosamente."
      redirect_to @ad
    else
      flash[:danger] = "Lamentablemente el anuncio no se ha podido publicar. Por favor corrija los siguientes errores."
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @ad.update(ad_params)
      flash[:success] = "Felicidades, su anuncio ha sido modificado exitosamente."
      redirect_to @ad
    else
      flash[:danger] = "Lamentablemente su anuncio no se ha podido publicar. Por favor corrija los siguientes errores."
      render 'edit'
    end
  end

  def destroy
    @ad.destroy
    flash[:info] = "El archivo titulado '#{@ad.name}' ha sido eliminado exitosamente."
    redirect_to ads_path
  end
  
private

  def ad_params
    params.require(:ad).permit(:name, :description, :section_id)
  end

  def set_ad
    @ad = Ad.find(params[:id])
  end

  def require_show_user
    unless ((current_user == @ad.user) || (current_user.sections.take!.id == @ad.section_id && (cannot? :create, Forum)))
      flash[:danger] = "You must be logged in to perform that action"
      redirect_to root_path
    end
  end
end
