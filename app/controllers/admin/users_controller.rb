class Admin::UsersController < Admin::AdminController
  
  require 'bcrypt'

  before_action :set_user, only: [:show, :edit, :update, :destroy, :reset_password]
  before_action :set_roles, only: [:show, :new, :create, :edit, :update]

  $num = 2
  $i = 0

  def index
    authorize! :read, User
    if params[:role] and params[:status]
      @users = User.where(status: params[:status], id: UserRole.where(role_id: Role.where(name: params[:role]).pluck(:id)).pluck(:user_id))
      @rol = Role.find_by_name(params[:role])
    elsif params[:role]
      @users = Role.find_by_name(params[:role]).users
      @rol = Role.find_by_name(params[:role])
      if (params[:role] == 'Representante') 
        @users = User.distinct.where(id: User.all.pluck(:representing_id_1))
        @rol = Role.find_by_name(params[:role])
      end
    elsif params[:status]
      @users = User.where(status: params[:status])
    else
      @users = User.all
    end
  end

  def show
    authorize! :read, User
    #@user.roles
    if @user.is_student? and @user.representing_id_1 != nil
      @user_representing = User.find(@user.representing_id_1)
    end
  end

  def new
    authorize! :create,User
    @user = User.new
  end

  def create
    authorize! :create, User
    @user = User.new(user_params)
    @user.password = 'cruzcarrillo123'
    if @user.representing_id_1
      u_r = User.where(id: @user.representing_id_1).pluck(:id)
      rol = Role.where(name: 'Representante').pluck(:id)
      if (UserRole.where(user_id: u_r[0], role_id: rol[0]).count == 0)
        ur = UserRole.new(user_id: u_r[0], role_id: rol[0])
        ur.save!
      end
    end
    if @user.save
      @message = "Felicidades, usted ha sido registrado exitosamente en el protal web del Colegio Cruz Carrillo, su usuario es: "+@user.email
      @subject = "Registro Colegio Cruz Carrillo: "+@user.name+" "+@user.lastname
      SendMessageToUserMailer.email(@user, @message, @subject).deliver_now
      @email = Email.new
      @email.subject = @subject
      @email.message = @message
      @email.receiver = @user.email
      @email.user_id = current_user.id
      @email.save
      flash[:success] = "El usuario ha sido creado exitosamente"
      redirect_to admin_users_path
    else
      flash.now[:danger] = @user.errors.full_messages
      render new_admin_user_path(@user)
    end
  end

  def edit
    authorize! :update, User
  end

  def update
    authorize! :update, User
    if $i < $num
      if(current_user.valid_password?(params[:user][:password]))
        if params[:user][:representing_id_1]
          u_r = User.where(id: params[:user][:representing_id_1]).pluck(:id)
          rol = Role.where(name: 'Representante').pluck(:id)
          if (UserRole.where(user_id: u_r[0], role_id: rol[0]).count == 0)
            ur = UserRole.new(user_id: u_r[0], role_id: rol[0])
            ur.save!
          end
        end
        @user.check_and_remove_representing_role(@user.id, params[:user][:representing_id_1])
        if @user.update_without_password(user_params)         
          flash[:success] = "El usuario ha sido modificado exitosamente "
          redirect_to admin_users_path
          $i = 0
        else
          flash.now[:warning] = @user.errors.full_messages
          render 'edit'
          $i += 1
        end
      else
        flash.now[:warning] = "Password administrador inválida"
        render 'edit'
        $i += 1
      end
    else
      flash[:danger] = "Máximo de intentos de cambios superado"
      redirect_to admin_users_path
      $i = 0
    end
  end

  def destroy
    authorize! :destroy, User
    if $i < $num
      if(current_user.valid_password?(params[:user][:password]))
        #eliminare usuario
        begin
          if @user.user_sections.count == 0 and @user.profesor_subject.count == 0
            @user.destroy
            flash[:danger] = "Usuario Eliminado del sistema"
            @user.remove_representing_role(@user.representing_id_1)
            redirect_to admin_users_path
            $i = 0
          else
            flash[:warning] = "Usuario no eliminado, existen registros asociados al registro que esta tratando de eliminar"
            redirect_to admin_users_path
          end        
        rescue ActiveRecord::InvalidForeignKey
          flash[:warning] = "Usuario no eliminado, existen registros asociados al registro que esta tratando de eliminar"
          redirect_to admin_users_path
        end
      else  
        flash[:warning] = "Password administrador invalida"
        redirect_to admin_users_path
        $i += 1
      end
    else
      flash[:danger] = "Maximo de intentos de cambios superado"
      redirect_to admin_users_path
      $i = 0
    end
  end

  def reset_password
    authorize! :reset_password, User
    if $i < $num
      if(current_user.valid_password?(params[:user][:password]))
        #eliminare usuario
      password = 'cruzcarrillo123'
      new_hashed_password = User.new(:password => password).encrypted_password
        @user.assign_attributes({:encrypted_password => new_hashed_password})
        if @user.save
          @message = @user.email+" Tiene un reseteo de clave realizado exitosamente, su clave es cruzcarrillo123 en el protal web del Colegio Cruz Carrillo, se recomienda ingresar al sistema y realizar el cambio de clave por seguridad."
          @subject = "Resteo Clave Colegio Cruz Carrillo: "+@user.name+" "+@user.lastname
          SendMessageToUserMailer.email(@user, @message, @subject).deliver_now
          @email = Email.new
          @email.subject = @subject
          @email.message = @message
          @email.receiver = @user.email
          @email.user_id = current_user.id
          @email.save
          flash[:success] = "Password reset realizado exitosamente"
          redirect_to admin_users_path
          $i = 0
        else
          flash[:warning] = @user.errors.full_messages
          redirect_to admin_users_path
          $i += 1
        end
      else  
        flash[:warning] = "Password administrador invalida"
        redirect_to admin_users_path
        $i += 1
      end
    else
      flash[:danger] = "Maximo de intentos de cambios superado"
      redirect_to admin_users_path
      $i = 0
    end
  end

  def send_email_representing
    authorize! :send_email_representing, User
    @message = params[:data_1]
    @subject = params[:data_2]
    @user_representing = User.find(params[:data_3])
    #puts 'Mensaje: '+@message.to_s+' Asunto: '+@subject.to_s+' Destinatario: '+@user_representing.to_s
    #SendMessageToUserMailer.email(@user_representing, @message, @subject).deliver_now
    begin
      SendMessageToUserMailer.email(@user_representing, @message, @subject).deliver_now
      @email = Email.new
      @email.subject = @subject
      @email.message = @message
      @email.receiver = @user_representing
      @email.user_id = current_user.id
      @email.save
      render :json => {:status => "Ok"}
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
      render :json => {:status => "NotOk"}
    end
  end

  def send_mm_email
    authorize! :send_mm_email, User
    @message = params[:data_1]
    @subject = params[:data_2]
    
    user_mail = params[:data_3]
    
    user_mail.each do |um|
      #puts "Correo: "+um.to_s
      SendEmailMailer.email(um, @message, @subject).deliver
      @email = Email.new
      @email.subject = @subject
      @email.message = @message
      @email.receiver = um
      @email.user_id = current_user.id
      @email.save
    end

    render :json => {:status => "Ok"}
  end

  def send_ms_email
    authorize! :send_ms_email
    @message = params[:data_1]
    @subject = params[:data_2]
    emails = Array.new
    
    params[:data_3].each do |section|
      #puts "Seccion_id: "+section.to_s
      
      a = User.joins(:user_sections).where('section_id = ?', section).pluck(:user_id)
      b = User.joins(:user_sections).where('section_id = ?', section).pluck(:representing_id_1)
      c = SectionProfesorSubject.joins(profesor_subject: [:subject, :user]).select("users.id").where('section_id = ?', section)

      a.each do |us|
        emails.push(User.find(us).email)
      end

      b.each do |us|
        emails.push(User.find(us).email)
      end

      c.each do |us|
         emails.push(User.find(us.id).email)
      end
    end
    emails = emails.uniq
    #puts 'Correo asociados a la seccion 1: '+emails.to_s
    emails.each do |um|
      SendEmailMailer.email(um, @message, @subject).deliver
      @email = Email.new
      @email.subject = @subject
      @email.message = @message
      @email.receiver = um
      @email.user_id = current_user.id
      @email.save
    end
    render :json => {:status => "Ok"}
  end

  private
    def user_params
      params.require(:user).permit(
        :name, :lastname, :dni, :birthdate, :gender, :address, :email, :status, :message, :representing_id_1, :cellphone, :cellphone2, role_ids: []
      )
    end

    def set_user
      if User.is_numeric(params[:id]) == true
        begin
          @user = User.find(params[:id])
        rescue Exception => e
          puts "Exception: "+e.to_s
          flash[:warning] = "Ocurrió un error en el sistema"
          redirect_to admin_users_path
        end
      else
        redirect_to to admin_users_path
      end
    end

    def set_roles
      begin
        @roles = Role.all
      rescue Exception => e
        puts "Exception: "+e.to_s
        flash[:warning] = "Ocurrió un error en el sistema"
        redirect_to admin_users_path
      end
    end

end