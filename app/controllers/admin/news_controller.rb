class Admin::NewsController < Admin::AdminController
  
  before_action :set_new, only: [:show, :edit, :update, :destroy]
  

  def index
    authorize! :read, New
    #@news_by_date = @news.group_by(&:date_init)
    if params[:status]
      @news = New.where("date_end < ?", DateTime.now.to_date)
    else
      @news = New.where("date_end >= ?", DateTime.now.to_date)
    end
  end

  def new  
    authorize! :read, New
    @new = New.new
  end

  def show
    authorize! :read, New
    @user_email = @new.user.email
  end

  def edit
    authorize! :update, New
  end

  def create
    authorize! :create, New
    @new = New.new(new_params)
    
    #@new.user_id = 1
    @new.assign_attributes({:user_id => current_user.id})
    if @new.save
      flash[:success] = "Anuncio ha sido creado exitosamente"
      redirect_to admin_news_index_path
    else
      flash.now[:danger] = "Anuncio no creado"
      redirect_to new_admin_news_path
    end
  end

  

  def update
    authorize! :update, New
    if @new.update(new_params)
      flash[:success] = "Anuncio modificado exitosamente"
      redirect_to admin_news_index_path
    else
      flash.now[:danger] = "Anuncio no modificado, por favor revise los campos"
      render 'edit'
    end
  end

  def destroy
    authorize! :destroy, New
    # if $i < $num
    #   user = User.find(current_user.id)
    #   if (user.valid_password?(params[:password]))
    #     #eliminare anuncio
    #     #@new.destroy
    #     flash[:warning] = "Anuncio eliminado del sistema"+(user.name)
    #     redirect_to admin_news_index_path
    #     $i = 0
    #   else  
    #     flash[:warning] = "Password invalida"+(user.name)
    #     redirect_to admin_news_index_path
    #     $i += 1
    #   end
    # else
    #   flash[:danger] = "Maximo de intentos de cambios superado"
    #   redirect_to admin_news_index_path
    #   $i = 0
    # end
    @new.destroy
    flash[:danger] = "Anuncio eliminado del sistema"
    redirect_to admin_news_index_path
  end

    

  private

    def set_new
      if New.is_numeric(params[:id]) == true
        begin
          @new = New.find(params[:id])
        rescue Exception => e
          puts "Exception: "+e.to_s
          flash[:warning] = "Ocurrió un error en el sistema"
          redirect_to admin_subjects_path(status: 'Activo')
        end
      else
        redirect_to admin_news_index_path
      end
    end

    def new_params
      params.require(:new).permit(:name, :description, :date_init, :date_end)
    end
end

