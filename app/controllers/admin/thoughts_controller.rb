class Admin::ThoughtsController < Admin::AdminController
  before_action :set_thought, only: [:show, :edit, :update, :destroy]

  # GET /thoughts
  # GET /thoughts.json
  def index
    authorize! :read, Thought
    if params[:status] == 'true'
      @thoughts = Thought.limit(1).order("RANDOM()")     
    else
      @thoughts = Thought.all
    end 
    
  end

  # GET /thoughts/1
  # GET /thoughts/1.json
  def show
    authorize! :read, Thought
  end

  # GET /thoughts/new
  def new
    authorize! :create, Thought
    @thought = Thought.new
  end

  # GET /thoughts/1/edit
  def edit
    authorize! :update, Thought
  end

  # POST /thoughts
  # POST /thoughts.json
  def create
    authorize! :create, Thought
    @thought = Thought.new(thought_params)

    if @thought.save
      flash[:success] = "Pensamiento ha sido creado exitosamente"
      redirect_to admin_thoughts_path
    else
      flash.now[:danger] = "Pensamiento no creado"
      redirect_to new_admin_thoughts_path
    end
  end

  # PATCH/PUT /thoughts/1
  # PATCH/PUT /thoughts/1.json
  def update
    authorize! :update, Thought
    if @thought.update(thought_params)
      flash[:success] = "Pensamiento modificado exitosamente"
      redirect_to admin_thoughts_path
    else
      flash.now[:danger] = "Pensamiento no modificado, por favor revise los campos"
      render 'edit'
    end
  end

  # DELETE /thoughts/1
  # DELETE /thoughts/1.json
  def destroy
    authorize! :destroy, Thought
    @thought.destroy
    flash[:danger] = "Pensamiento eliminado del sistema"
    redirect_to admin_thoughts_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thought
      begin
        @thought = Thought.find(params[:id])
      rescue Exception => e
        puts "Exception: "+e.to_s
        flash[:warning] = "Ocurrió un error en el sistema"
        redirect_to admin_thoughts_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def thought_params
      params.require(:thought).permit(:thought, :author)
    end
end
