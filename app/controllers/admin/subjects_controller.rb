class Admin::SubjectsController < Admin::AdminController
  before_action :set_subject, only: [:show, :edit, :update, :destroy]

  # GET /subjects
  # GET /subjects.json
  def index
    authorize! :read, Subject
    if params[:status]
      @subjects = Subject.where(status: params[:status])
      @profesor_subject = ProfesorSubject.all
      @users = User.where(id: ProfesorSubject.all.pluck(:user_id)).where(id: Role.find_by_name("Profesor").users.pluck(:user_id))
    else
      @subjects = Subject.all
      @profesor_subject = ProfesorSubject.all
      @users = User.where(id: ProfesorSubject.all.pluck(:user_id)).where(id: Role.find_by_name("Profesor").users.pluck(:user_id))
    end    
  end

  # GET /subjects/1
  # GET /subjects/1.json
  def show
    authorize! :read, Subject
    @profesor_subject = Subject.find(@subject.id).users
  end

  # GET /subjects/new
  def new
    authorize! :create, Subject
    @subject = Subject.new
  end

  # GET /subjects/1/edit
  def edit
    authorize! :update, Subject
    @users_section = Subject.find(@subject.id).users
    @users = User.where(status: 'Activo').where.not(id: ProfesorSubject.all.pluck(:user_id)).where(id: Role.find_by_name("Profesor").users.pluck(:user_id)) - Subject.find(@subject.id).users
  end

  # POST /subjects
  # POST /subjects.json
  def create
    authorize! :create, Subject
    @subject = Subject.new(subject_params)

    if @subject.save
      flash[:success] = "Materia ha sido creada exitosamente"
      redirect_to admin_subjects_path(status: @subject.status)
    else
      flash[:danger] = "Materia no creada"
      redirect_to new_admin_subjects_path
    end
  end

  # PATCH/PUT /subjects/1
  def update
    authorize! :update, Subject
    begin
      if @subject.update(subject_params)
        flash[:success] = "Materia modificada exitosamente"
        redirect_to admin_subjects_path(status: @subject.status)
      else
        flash[:danger] = "Materia no modificada, por favor revise los campos"
        render 'edit'
      end
    rescue ActiveRecord::InvalidForeignKey
      flash[:danger] = "Materia no modificada, existen registros asociados al registro que esta tratando de modificar"
      redirect_to admin_subjects_path(status: @subject.status)
    end
  end

  # DELETE /subjects/1
  # DELETE /subjects/1.json
  def destroy
    authorize! :destroy, Subject
    if @subject.check_users_subject == 1
      begin
        @subject.destroy
        flash[:danger] = "Materia eliminada del sistema"
        redirect_to admin_subjects_path(status: @subject.status)
      rescue ActiveRecord::InvalidForeignKey
        flash[:danger] = "Materia no eliminada, existen registros asociados al registro que esta tratando de modificar"
        redirect_to admin_subjects_path(status: @subject.status)
      end
    else
      flash[:warning] = "Materia no eliminada del sistema, existen registros asociados"
      redirect_to admin_subjects_path(status: @subject.status)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      if Subject.is_numeric(params[:id]) == true
        begin
          @subject = Subject.find(params[:id])
        rescue Exception => e
          puts "Exception: "+e.to_s
          flash[:warning] = "Ocurrió un error en el sistema"
          redirect_to admin_subjects_path(status: 'Activo')
        end
      else
        redirect_to admin_subjects_path(status: 'Activo')
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_params
      params.require(:subject).permit(:name, :description, :status, user_ids: [])
    end
end
