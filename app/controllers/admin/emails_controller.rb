class Admin::EmailsController < Admin::AdminController
  before_action :set_mail, only: [:show, :destroy]

  def index
    authorize! :read, Email
  	@emails = Email.all
  end
  
  def destroy
    authorize! :destroy, New
  	@email.destroy
    flash[:danger] = "Email eliminado del sistema"
    redirect_to admin_emails_path
  end

  private
  def set_mail
    begin
      @email = Email.find(params[:id])
    rescue Exception => e
      puts "Exception: "+e.to_s
      flash[:warning] = "Ocurrió un error en el sistema"
      redirect_to admin_emails_path
    end
  end
end
