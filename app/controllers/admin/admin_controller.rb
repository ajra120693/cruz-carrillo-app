class Admin::AdminController < ApplicationController
	before_action :authenticate_user! #Este pronto con el can can can
  	layout 'admin'

	def check_mode
		begin
			if User.find(current_user.id).mode == 'Oscuro'
				#return mode = 'Oscuro'
				render :json => {:mode => "Oscuro"}
			else 
				render :json => {:mode => 'Blanco'}
			end
		rescue
			render :json => {:mode => 'Blanco'}
		end
	end

	def update_mode
		mode = params[:mode]
		if mode = 'Oscuro'
			begin
				@user = User.find(current_user.id)
				@user.mode = params[:mode]
				@user.save
				render :json => {:status => 'OK'}
			rescue Exception
			  	# handle everything else
			 	render :json => {:status => 'NotOK'}
			end
		else
			begin
				@user = User.find(current_user.id)
				@user.mode = 'Blanco'
				@user.save
				render :json => {:status => 'OK'}
			rescue Exception
			  	# handle everything else
			 	render :json => {:status => 'NotOK'}
			end
		end
	end
end
