class Admin::SectionsController < Admin::AdminController
  before_action :set_section, only: [:show, :edit, :update, :destroy]

  # GET /sections
  # GET /sections.json
  def index
    authorize! :read, Section
    @sections = Section.all
    @users_section = UserSection.all
    @users = User.where(id: UserSection.all.pluck(:user_id)).where(id: Role.find_by_name("Estudiante").users.pluck(:user_id))
  end

  # GET /sections/1
  # GET /sections/1.json
  def show
    authorize! :read, Section
    @users_section = Section.find(@section.id).users
    @sps = SectionProfesorSubject.joins(profesor_subject: [:subject, :user]).select("section_profesor_subjects.*, subjects.name as sbname, users.name, users.lastname").where(:section_id => @section.id)
    # https://guides.rubyonrails.org/v5.1/active_record_querying.html#retrieving-specific-data-from-multiple-tables de aqui saque el query con ORM
  end

  def students_users_with_not_section
    authorize! :read, Section
    render :json => ( User.where.not(id: UserSection.all.pluck(:user_id)).where(id: Role.find_by_name("Estudiante").users.pluck(:user_id), status: 'Activo') )
  end

  def professors_with_subjects
    authorize! :read, Section
    #render :json => (Subject.select("subjects.name as subjectname, users.name as username, users.lastname as userlastname, profesor_subjects.id as idps").joins(:users))
    render :json => (Subject.select("subjects.name as subjectname, users.name as username, users.lastname as userlastname, users.dni as userdni, profesor_subjects.id as p_s_id ,profesor_subjects.*").joins(:users).where("subjects.status = ? ", "Activo")) 
  end

  def section_professor_consult
    authorize! :read, Section
    render :json => (SectionProfesorSubject.where(:profesor_subject_id => params[:id]))
  end

  # GET /sections/new
  def new
    authorize! :create, Section
    @students_users_with_not_section =  User.where.not(id: UserSection.all.pluck(:user_id)).where(id: Role.find_by_name("Estudiante").users.pluck(:user_id)) 
    @section = Section.new
    if @section.id
      @users_section = Section.find(@section.id).users
    else
      @users_section = nil
    end
  end

  # GET /sections/1/edit
  def edit
    authorize! :update, Section
    @users_section = Section.find(@section.id).users
    #@users = Role.find_by_name("Estudiante").users - Section.find(@section.id).users
    @sps = SectionProfesorSubject.joins(profesor_subject: [:subject, :user]).select("section_profesor_subjects.*, subjects.name as sbname, users.name, users.lastname").where(:section_id => @section.id)
    @users = User.where.not(id: UserSection.all.pluck(:user_id)).where(id: Role.find_by_name("Estudiante").users.pluck(:user_id)) - Section.find(@section.id).users
  end

  # POST /sections
  # POST /sections.json
  def create
    authorize! :create, Section
    @section = Section.new(section_params)
    if params[:section_profesor_subjects]
      if @section.save

        $i
        @profesor_subject_id
        @day
        @time_init
        @time_end

        params[:section_profesor_subjects].each do |spsa|
          $i = spsa.split(',').length
          @profesor_subject_id = (spsa.split(',')[$i-$i]).to_i
          @day = (spsa.split(',')[$i - ($i - 1)]).to_s
          @time_init = (spsa.split(',')[$i - ($i - 2)]).to_f
          @time_end = (spsa.split(',')[$i - ($i - 3)]).to_f

          SectionProfesorSubject.create!(:profesor_subject_id => @profesor_subject_id, :section_id => @section.id, :day => @day, :time_init => @time_init, :time_end => @time_end)
        end

        flash[:success] = "Seccion Creada Exitosamente"
        redirect_to admin_sections_path
      else
        flash[:danger] = @section.errors.full_messages
        render :new
      end  
    else
      flash[:danger] = "No se encontraron materias para agregar a la seccion"
      render :new
    end  
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    authorize! :update, Section
    #respond_to do |format|
    if params[:section_profesor_subjects]
      if @section.update(section_params)
        SectionProfesorSubject.where(:section_id => @section.id).destroy_all
        $i
        @profesor_subject_id
        @day
        @time_init
        @time_end

        params[:section_profesor_subjects].each do |spsa|
          $i = spsa.split(',').length
          @profesor_subject_id = (spsa.split(',')[$i-$i]).to_i
          @day = (spsa.split(',')[$i - ($i - 1)]).to_s
          @time_init = (spsa.split(',')[$i - ($i - 2)]).to_f
          @time_end = (spsa.split(',')[$i - ($i - 3)]).to_f
          SectionProfesorSubject.create!(:profesor_subject_id => @profesor_subject_id, :section_id => @section.id, :day => @day, :time_init => @time_init, :time_end => @time_end)
        end

        flash[:success] = "Seccion Editada Exitosamente"
        redirect_to admin_sections_path
        # format.html { redirect_to @section, notice: 'Section was successfully updated.' }
        # format.json { render :show, status: :ok, location: @section }
      else
        flash[:danger] = @section.errors.full_messages
        render :edit
        # format.html { render :edit }
        # format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    else
      flash[:danger] = "No se encontraron materias para agregar a la seccion"
      render :edit
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    authorize! :destroy, Section
    if @section.check_users_section == 1  
      if @section.destroy
        SectionProfesorSubject.where(:section_id => @section.id).destroy_all
        redirect_to admin_sections_path
      else
        redirect_to admin_sections_path
      end
    else
      flash[:warning] = "Seccion no eliminada del sistema, existen registros asociados"
      redirect_to admin_sections_path
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section
      begin
        @section = Section.find(params[:id])
      rescue Exception => e
        puts "Exception: "+e.to_s
        flash[:warning] = "Ocurrió un error en el sistema"
        redirect_to admin_sections_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_params
      params.require(:section).permit(:description, user_ids: [])
    end
end
