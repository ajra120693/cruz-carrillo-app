class CommentsController < Admin::AdminController
  def new
    @forum = Forum.find(params[:forum_id])
    @comment = @forum.comments.create(comment_params)
    @comment.user_id = current_user.id    
  end
  
  def create
    @forum = Forum.find(params[:forum_id])
    @comment = @forum.comments.create(comment_params)
    @comment.user_id = current_user.id
    @comment.save
    redirect_to @forum
  end

  def destroy
    @forum = Forum.find(params[:forum_id])
    @comment = @forum.comments.find(params[:id])
    @comment.destroy
    redirect_to @forum
  end

private
  def comment_params
    params.require(:comment).permit(:commenter, :body)
  end
end
