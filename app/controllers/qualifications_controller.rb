class QualificationsController < Admin::AdminController
  before_action :set_qualification, only: [:show, :edit, :update]
 
  def index
    @qualifications = Qualification.order('created_at DESC')
  end
 
  def show
  end
 
  def new
    @qualification = Qualification.new
  end
 
  def create
    @qualification = Qualification.new(qualification_params)
    if @qualification.save
      redirect_to qualifications_path
    else
      render :new
    end
  end
 
  def edit
  end
 
  def update
    if @qualification.update_attributes(qualification_params)
      redirect_to qualification_path(@qualification)
    else
      render :edit
    end
  end
 
  private
 
  def qualification_params
    params.require(:qualification).permit(:title, :body, :image)
  end
 
  def set_qualification
    @qualification = Qualification.find(params[:id])
  end
end