class RepliesController < ApplicationController
  def create
    @comment = Comment.find(params[:article_id])
    @reply = @comment.replies.create(comment_params)
    redirect_to comment_path(@comment)
  end

  private
  def reply_params
    params.require(:reply).permit(:body)
  end


end
