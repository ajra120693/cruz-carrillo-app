class SendMessageToUserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.send_message_to_user_mailer.email.subject
  #
  def email(user, message, subject)
    
    @user = user
    @subject = subject
    @message = message

    mail to: @user.email, subject: @subject
  end

end
