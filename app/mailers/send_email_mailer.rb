class SendEmailMailer < ApplicationMailer
	def email(email, message, subject)
    
    @email = email
    @subject = subject
    @message = message

    mail to: @email, subject: @subject
  end
end
