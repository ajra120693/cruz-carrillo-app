class ApplicationMailer < ActionMailer::Base
  default from: "'Cruz Carrillo' <colegiocruzcarrilloweb@gmail.com>"
  layout 'mailer'
end
