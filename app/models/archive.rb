class Archive < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :section, optional: true


  mount_uploader :attachment, AttachmentUploader # Tells rails to use this uploader for this model.
  validates :name, presence: { message: "debe existir" },
                  length: {in: 5..80, 
                  too_short: "debe tener al menos 5 carácteres.", 
                  too_long: "debe tener máximo 80 carácteres."}

  validates :attachment, presence: {message: 'Debe cargar el archivo.'}, 
  file_size: { less_than: 1.gigabytes, message: 'El archivo no puede ser muy pesado.'}   
  
  validates :section_id, presence: {message: 'debe existir.'}

  def to_s
		self.description    #pudiera colocar más información self.titulo+ “ | “ + self.contenio
  end
  
end