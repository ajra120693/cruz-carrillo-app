class Subject < ApplicationRecord

	has_many :profesor_subject, dependent: :destroy
  	has_many :users, through: :profesor_subject

	def self.is_numeric(number)
  	return true if number =~ /^\d+$/
  	true if Float(number) rescue false
	end

	validates :name, presence: {message: "can't be blank"}, length: { maximum: 50, too_long: "%{count} caracteres es el máximo permitido"}
	validates :description, presence: {message: "can't be blank"}, length: { maximum: 50, too_long: "%{count} caracteres es el máximo permitido"}
	validates :status, presence: {message: "can't be blank"}, length: { maximum: 50, too_long: "%{count} caracteres es el máximo permitido"}

	def check_users_subject
		puts 'Entre'
		puts 'modelo: '+self.users.count.to_s
		status = self.status.to_s
		if self.users.count == 0
			return 1
		else 
			return 0
		end
	end
end	
