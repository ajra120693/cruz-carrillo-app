class Forum < ApplicationRecord
  has_many :comments , :dependent => :destroy
  belongs_to :user, optional: true
  belongs_to :section, optional: true

  validates :title, presence: {message: 'debe existir'},
                    length: {in: 5..80, 
                    too_short: "debe tener al menos 5 carácteres."}

  validates :comment, presence: { message: "debe existir" },
                          length: {in: 10..1000, 
                          too_short: "debe tener al menos 10 carácteres."}

  validates :section_id, presence: { message: "debe existir" }

  def current_time
    Time.zone.now.to_datetime.strftime("%Y-%m-%d-%H%M%S")
  end
end