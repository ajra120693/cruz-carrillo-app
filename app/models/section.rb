class Section < ApplicationRecord
	#Users
	has_many :user_sections, dependent: :destroy
	has_many :users, through: :user_sections

	#Profesores y Materias
	has_many :section_profesor_subjects, dependent: :destroy
	has_many :profesor_subjects, through: :section_profesor_subjects

	# Foros
	has_many :forums, dependent: :destroy
	# Anuncios
	has_many :ads, dependent: :destroy
	# Archivos
	has_many :archives, dependent: :destroy

	validates :description, presence: { message: 'La seccion debe de tener una descripcion' }

	def to_s
		self.description    #pudiera colocar más información self.titulo+ “ | “ + self.contenio
	end

	def check_users_section
		puts 'Entre'
		puts 'modelo: '+self.users.count.to_s
		if self.users.count == 0 and self.profesor_subjects.count == 0
			return 1
		else 
			return 0
		end
	end
end
