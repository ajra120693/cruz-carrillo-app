class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

    if user.is_admin?
      can :manage, :all
    else

      #can :read, :all

      if user.has_role? "Profesor"
        can :manage, Forum
        cannot :manage, User
        cannot :manage, Section
        cannot :manage, Thought
        cannot :manage, Subject
        cannot :manage, SectionProfesorSubject
        cannot :manage, New
        cannot :manage, Email
        can :reset_password, User
        can :send_mm_email, User
        can :send_ms_email, User
      # else
      #   can :read, Forum
      end

      if user.has_role? "Administrativo"
        can [:create, :read, :update], [User, Section, Thought, Subject, SectionProfesorSubject, New]
        can :destroy, [Thought, Subject, New, Section]
        cannot :manage, [Forum, Ad, Archive]
        cannot :destroy, [User]
        can :students_users_with_not_section, Section
        can :professors_with_subjects, Section
        can :section_professor_consult, Section
        can :send_email_representing, User
        can :reset_password, User
        can :send_mm_email, User
        can :send_ms_email, User
        can :manage, Email
      end

      if user.has_role? "Representante"
        cannot :manage, User
        cannot :manage, Section
        cannot :manage, Thought
        cannot :manage, Subject
        cannot :manage, SectionProfesorSubject
        cannot :manage, New
        cannot :send_mm_email, User
        cannot :send_ms_email, User
        cannot :manage, Email
      end

      if user.has_role? "Estudiante"
        cannot :manage, User
        cannot :manage, Section
        cannot :manage, Thought
        cannot :manage, Subject
        cannot :manage, SectionProfesorSubject
        cannot :manage, New
        can :read, Forum
        cannot :send_mm_email, User
        cannot :send_ms_email, User
        cannot :manage, Email
      end


    end



    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
