class Role < ApplicationRecord

	has_many :user_roles, dependent: :destroy
	has_many :users, through: :user_roles

	SUPERADMIN = 'Superadmin'
	ESTUDIANTE = 'Estudiante'
	PROFESOR = 'Profesor'
	ADMINISTRATIVO = 'Administrativo'
	REPRESENTANTE = 'Representante'

	ROLES = [
		SUPERADMIN,
		ESTUDIANTE,
		PROFESOR,
		ADMINISTRATIVO,
		REPRESENTANTE
	]

end
