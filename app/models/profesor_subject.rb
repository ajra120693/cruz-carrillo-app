class ProfesorSubject < ApplicationRecord
  belongs_to :user
  belongs_to :subject


  has_many :section_profesor_subjects, dependent: :destroy
  has_many :sections, through: :section_profesor_subjects
end
