class User < ApplicationRecord
  	
	#Roles
	has_many :user_roles, dependent: :destroy
	has_many :roles, through: :user_roles

	#News
	has_many :news

	#Secciones
	has_many :user_sections, dependent: :destroy
	has_many :sections, through: :user_sections

	#Materias
	has_many :profesor_subject, dependent: :destroy
	has_many :subjects, through: :profesor_subject

	#Reprsentantes y Estudiantes
	has_many :student, :class_name => 'StudentRepresenting', :foreign_key => 'student_id'
	has_many :representing, :class_name => 'StudentRepresenting', :foreign_key => 'representing_id'

	# Publicacion de profesores
	has_many :forums, dependent: :destroy

	# Comentario de estudiantes
	has_many :comments, dependent: :destroy

	# Anuncios de profesores
	has_many :ads

	# Emails
	has_many :emails	

	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
	devise :database_authenticatable, :registerable,
	     :recoverable, :rememberable, :validatable

	validates :password, :presence => true, :on => :create

	def self.is_numeric(number)
	  	return true if number =~ /^\d+$/
	  	true if Float(number) rescue false
	end

	EMAIL_FORMAT = /\A(?!.*\.\.)[a-z\d_.\-+]{1,50}@[a-z\d\-]{1,50}\.[a-z]{1,30}\z/i
	PASSWORD_FORMAT = /\A(?=.*[A-Z])(?=.*\d)(?=.*(?:!|#|\$|%|&|\/|\(|\)|=|\?|\*|\.)).{8,32}\z/

  	validates_format_of :email, with: EMAIL_FORMAT

	def has_role?(role_name)
    	roles.any? {|role| role.name == role_name.capitalize}
	end

	def is_admin?
		has_role?("Superadmin")
	end

	def is_administrativo?
		has_role?("Administrativo")
	end

	def is_profesor?
		has_role?("Profesor")
	end

	def is_student?
		has_role?("Estudiante")
	end

  	def is_representing?
    	has_role?("Representante")
	end
	
	# Gravatar

	def avatar
		# include MD5 gem, should be part of standard ruby install
		# require 'digest/md5'
		
		# get the email from URL-parameters or what have you and make lowercase
		email_address = self.email.downcase
		
		# create the md5 hash
		hash = Digest::MD5.hexdigest(email_address)
		
		# compile URL which can be used in <img src="RIGHT_HERE"...
		image_src = "https://www.gravatar.com/avatar/#{hash}"	
	end

	validates :name, presence: {message: "Nombre no puede estar vacío"}, length: { maximum: 100, too_long: "%{count} caracteres es el máximo permitido"}
	validates :lastname, presence: {message: "Apellido no puede estar vacío"}, length: { maximum: 100, too_long: "%{count} caracteres es el máximo permitido"}
	validates :dni, presence: {message: "DNI no puede estar vacío"}, length: { maximum: 10, too_long: "%{count} caracteres es el máximo permitido"}, uniqueness: true, length: { minimum: 6, :too_short => '%{count} caracteres es el mínimo permitido'}
	validates :email, presence: {message: "Email no puede estar vacío"}, length: { maximum: 50, too_long: "%{count} caracteres es el máximo permitido"}, uniqueness: true
	validates :birthdate, presence: {message: "Fecha de Nacimiento no puede estar vacío"}
	validates :gender, presence: {message: "Seleccione género"}
	validates :address, presence: {message: "Dirección no puede estar vacía"}, length: { maximum: 100, too_long: "%{count} caracteres es el máximo permitido"}
	validates :status, presence: {message: "Seleccione status"}
	validates :cellphone, presence: {message: "Teléfono no puede estar vacío"}, length:{ :within => 11..16, :too_short => '%{count} caracteres es el mínimo permitido', :too_long => '%{count} caracteres es el máximo permitido'}
	validates :cellphone2, length:{ :within => 11..16, :too_short => '%{count} caracteres es el mínimo permitido', :too_long => '%{count} caracteres es el máximo permitido'}, allow_blank: true


	def check_and_remove_representing_role(user_id, representing_id_1)
		u = User.find(user_id)
		puts 'user_id: '+user_id.to_s+' representing_id_1: '+representing_id_1.to_s
		puts 'Usuario: '+u.representing_id_1.to_s
		if u.representing_id_1.to_s != representing_id_1.to_s
			puts 'Usuarios dintintos'
			if User.where(representing_id_1: u.representing_id_1).count == 1
				puts 'Usuario no tiene mas alumnos asociados se procede a quitar rol de Representante'
				u_r = UserRole.where(user_id: u.representing_id_1, role_id: Role.where(name: 'Representante').pluck(:id))
				puts 'Id de lo que se eliminará: '+u_r.ids.to_s
				UserRole.destroy(u_r.ids)
			end
		end	
  	end

  	def remove_representing_role(representing_id_1)	
  		if User.where(representing_id_1: representing_id_1).count == 0
			@u_r = UserRole.where(user_id: u.representing_id_1, role_id: Role.where(name: 'Representante').pluck(:id))
			UserRole.destroy(u_r.ids)
		end
  	end
  	
  	def check_is_user_is_in_section_or_subject
  		if self.profesor_subject.count == 0 and self.user_sections.count == 0
  			return 1
  		else
  			return 0
  		end
  	end
end

