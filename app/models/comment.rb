class Comment < ApplicationRecord
  belongs_to :forum
  belongs_to :user, optional: true

  validates :body, presence: {message: 'Debe existir el comentario'}

  def current_time
    Time.zone.now.to_datetime.strftime("%Y-%m-%d-%H%M%S")
  end
end
