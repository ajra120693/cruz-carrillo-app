class Ad < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :section, optional: true


  validates :name, presence: {message: 'debe existir'},
                  length: {in: 5..80, 
                  too_short: "debe tener al menos 5 carácteres.", 
                  too_long: "debe tener máximo 80 carácteres."}

  validates :description, presence: { message: "debe existir" },
                  length: {in: 10..100000, 
                  too_short: "debe tener al menos 10 carácteres.", 
                  too_long: "debe tener máximo 100000 carácteres."}

  validates :section_id, presence: { message: "debe existir" }
end