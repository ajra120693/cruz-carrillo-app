class Thought < ApplicationRecord
	validates :author, presence: {message: "Autor no puede estar vacío"}, length: { maximum: 50, too_long: "%{count} caracteres es el máximo permitido"}
	validates :thought, presence: {message: "Pensamiento no puede estar vacío"}, length: { maximum: 350, too_long: "%{count} caracteres es el máximo permitido"}
end
