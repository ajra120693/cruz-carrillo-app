# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_25_015855) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ads", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "section_id"
    t.bigint "user_id"
    t.index ["section_id"], name: "index_ads_on_section_id"
    t.index ["user_id"], name: "index_ads_on_user_id"
  end

  create_table "archives", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "attachment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "section_id"
    t.index ["section_id"], name: "index_archives_on_section_id"
    t.index ["user_id"], name: "index_archives_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "commenter"
    t.text "body"
    t.bigint "forum_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["forum_id"], name: "index_comments_on_forum_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "emails", force: :cascade do |t|
    t.string "subject"
    t.string "message"
    t.string "receiver"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_emails_on_user_id"
  end

  create_table "forums", force: :cascade do |t|
    t.text "comment"
    t.text "reply"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.date "limit_date"
    t.string "qualification"
    t.integer "number"
    t.bigint "user_id"
    t.bigint "section_id"
    t.index ["section_id"], name: "index_forums_on_section_id"
    t.index ["user_id"], name: "index_forums_on_user_id"
  end

  create_table "news", force: :cascade do |t|
    t.text "name"
    t.text "description"
    t.date "date_init"
    t.date "date_end"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.index ["user_id"], name: "index_news_on_user_id"
  end

  create_table "personals", force: :cascade do |t|
    t.string "tilte"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profesor_subjects", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subject_id"], name: "index_profesor_subjects_on_subject_id"
    t.index ["user_id"], name: "index_profesor_subjects_on_user_id"
  end

  create_table "qualifications", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "replies", force: :cascade do |t|
    t.string "replier"
    t.text "body"
    t.bigint "comment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["comment_id"], name: "index_replies_on_comment_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "section_profesor_subjects", force: :cascade do |t|
    t.bigint "profesor_subject_id"
    t.bigint "section_id"
    t.string "day"
    t.decimal "time_init"
    t.decimal "time_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["profesor_subject_id"], name: "index_section_profesor_subjects_on_profesor_subject_id"
    t.index ["section_id"], name: "index_section_profesor_subjects_on_section_id"
  end

  create_table "sections", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_sections_on_user_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
  end

  create_table "thoughts", force: :cascade do |t|
    t.string "thought", limit: 350
    t.string "author"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_user_roles_on_role_id"
    t.index ["user_id"], name: "index_user_roles_on_user_id"
  end

  create_table "user_sections", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "section_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["section_id"], name: "index_user_sections_on_section_id"
    t.index ["user_id"], name: "index_user_sections_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name"
    t.string "lastname"
    t.string "dni"
    t.date "birthdate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "address"
    t.string "gender"
    t.string "status"
    t.string "message"
    t.integer "representing_id_1"
    t.string "cellphone"
    t.string "cellphone2"
    t.string "mode"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "ads", "sections"
  add_foreign_key "ads", "users"
  add_foreign_key "archives", "sections"
  add_foreign_key "archives", "users"
  add_foreign_key "comments", "forums"
  add_foreign_key "comments", "users"
  add_foreign_key "emails", "users"
  add_foreign_key "forums", "sections"
  add_foreign_key "forums", "users"
  add_foreign_key "news", "users"
  add_foreign_key "profesor_subjects", "subjects"
  add_foreign_key "profesor_subjects", "users"
  add_foreign_key "replies", "comments"
  add_foreign_key "section_profesor_subjects", "profesor_subjects"
  add_foreign_key "section_profesor_subjects", "sections"
  add_foreign_key "sections", "users"
  add_foreign_key "user_roles", "roles"
  add_foreign_key "user_roles", "users"
  add_foreign_key "user_sections", "sections"
  add_foreign_key "user_sections", "users"
end
