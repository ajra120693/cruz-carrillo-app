class AddSectionToArchives < ActiveRecord::Migration[5.2]
  def change
    add_reference :archives, :section, foreign_key: true
  end
end
