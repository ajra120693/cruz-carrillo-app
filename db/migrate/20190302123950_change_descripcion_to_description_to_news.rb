class ChangeDescripcionToDescriptionToNews < ActiveRecord::Migration[5.2]
  def change
  	rename_column :news, :descripcion, :description
  end
end
