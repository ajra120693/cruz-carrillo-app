class AddModeToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :mode, :string
  end
end
