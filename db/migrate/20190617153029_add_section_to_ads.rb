class AddSectionToAds < ActiveRecord::Migration[5.2]
  def change
    add_reference :ads, :section, foreign_key: true
  end
end
