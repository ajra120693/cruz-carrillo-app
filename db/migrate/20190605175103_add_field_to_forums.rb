class AddFieldToForums < ActiveRecord::Migration[5.2]
  def change
    add_column :forums, :title, :string
    add_column :forums, :limit_date, :date
    add_column :forums, :qualification, :string
    add_column :forums, :number, :integer
  end
end
