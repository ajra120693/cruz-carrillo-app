class AddSectionToForums < ActiveRecord::Migration[5.2]
  def change
    add_reference :forums, :section, foreign_key: true
  end
end
