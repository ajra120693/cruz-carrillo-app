class CreateSectionProfesorSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :section_profesor_subjects do |t|
      t.references :profesor_subject, foreign_key: true
      t.references :section, foreign_key: true
      t.string :day
      t.decimal :time_init
      t.decimal :time_end

      t.timestamps
    end
  end
end
