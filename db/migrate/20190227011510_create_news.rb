class CreateNews < ActiveRecord::Migration[5.2]
  def change
    create_table :news do |t|
      t.text :name
      t.text :descripcion
      t.date :date_init
      t.date :date_end
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
