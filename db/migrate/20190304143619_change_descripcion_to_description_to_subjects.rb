class ChangeDescripcionToDescriptionToSubjects < ActiveRecord::Migration[5.2]
  def change
  	rename_column :subjects, :descripcion, :description
  end
end
