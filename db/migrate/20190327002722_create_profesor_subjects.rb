class CreateProfesorSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :profesor_subjects do |t|
      t.references :user, foreign_key: true
      t.references :subject, foreign_key: true

      t.timestamps
    end
  end
end
