class CreateForums < ActiveRecord::Migration[5.2]
  def change
    create_table :forums do |t|
      t.text :comment
      t.text :reply

      t.timestamps
    end
  end
end
