class AddCellphoneToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :cellphone, :string
    add_column :users, :cellphone2, :string
  end
end
