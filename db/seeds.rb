# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
ActiveRecord::Base.transaction do
	
	puts 'Roles'
	roles = Role::ROLES
	roles.each do |role|
	  Role.find_or_create_by!(name: role)
	end

	puts 'Admin'
	PASSWORD = 'admin'
	admin = User.new(email: 'admin@cruzcarrillo.com', name: 'Admin Name', lastname: 'Admin lastname', dni:'000001', birthdate: '2004-06-07', gender:'Masculino', address:'Direccion', status:'Activo', cellphone:'04158524178', password: PASSWORD, password_confirmation: PASSWORD)
	admin.save!(validate: false) unless User.first

	puts 'Admin\'s Role'
	admin.role_ids = [Role.where(name: "Superadmin").take!.id]
	admin.save!(validate: false) unless User.first


	# PASSWORD2 = '000000'

	# user12 = User.new(email: 'prf1@cruzcarrillo.com', name: 'Profesor 1', lastname: 'Apellido Profesor 1', dni:'0000012', birthdate: '2004-06-07', gender:'Masculino', address:'Direccion', status:'Activo', cellphone:'04158524178', message: 'Menaje Profesor', password: PASSWORD2, password_confirmation: PASSWORD2)
	# user12.save()
	# user12.role_ids = [Role.where(name: "Profesor").take!.id]
	# user12.save()

	# user13 = User.new(email: 'prf2@cruzcarrillo.com', name: 'Profesor 2', lastname: 'Apellido Profesor 2', dni:'0000013', birthdate: '2004-06-07', gender:'Masculino', address:'Direccion', status:'Activo', cellphone:'04158524178', message: 'Menaje Profesor', password: PASSWORD2, password_confirmation: PASSWORD2)
	# user13.save()
	# user13.role_ids = [Role.where(name: "Profesor").take!.id]
	# user13.save()

	# user14 = User.new(email: 'prf3@cruzcarrillo.com', name: 'Profesor 3', lastname: 'Apellido Profesor 3', dni:'0000014', birthdate: '2004-06-07', gender:'Masculino', address:'Direccion', status:'Activo', cellphone:'04158524178', message: 'Menaje Profesor', password: PASSWORD2, password_confirmation: PASSWORD2)
	# user14.save()
	# user14.role_ids = [Role.where(name: "Profesor").take!.id]
	# user14.save()

	# user15 = User.new(email: 'prf4@cruzcarrillo.com', name: 'Profesor 4', lastname: 'Apellido Profesor 4', dni:'0000015', birthdate: '2004-06-07', gender:'Masculino', address:'Direccion', status:'Activo', cellphone:'04158524178', message: 'Menaje Profesor', password: PASSWORD2, password_confirmation: PASSWORD2)
	# user15.save()
	# user15.role_ids = [Role.where(name: "Profesor").take!.id]
	# user15.save()

	# user16 = User.new(email: 'prf5@cruzcarrillo.com', name: 'Profesor 5', lastname: 'Apellido Profesor 5', dni:'0000016', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', message: 'Menaje Profesor', password: PASSWORD2, password_confirmation: PASSWORD2)
	# user16.save()
	# user16.role_ids = [Role.where(name: "Profesor").take!.id]
	# user16.save()

	# user2 = User.new(email: 'est1@cruzcarrillo.com', name: 'Estudiante 1', lastname: 'Apellido 1', dni:'000002', birthdate: '2004-06-07', gender:'Masculino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user13.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user2.save()
	# user2.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user2.save()

	# user3 = User.new(email: 'est2@cruzcarrillo.com', name: 'Estudiante 2', lastname: 'Apellido 2', dni:'000003', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user14.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user3.save()
	# user3.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user3.save()

	# user4 = User.new(email: 'est3@cruzcarrillo.com', name: 'Estudiante 3', lastname: 'Apellido 3', dni:'000004', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user15.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user4.save()
	# user4.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user4.save()

	# user5 = User.new(email: 'est4@cruzcarrillo.com', name: 'Estudiante 4', lastname: 'Apellido 4', dni:'000005', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user15.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user5.save()
	# user5.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user5.save()

	# user6 = User.new(email: 'est5@cruzcarrillo.com', name: 'Estudiante 5', lastname: 'Apellido 5', dni:'000006', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user14.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user6.save()
	# user6.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user6.save()

	# user7 = User.new(email: 'est6@cruzcarrillo.com', name: 'Estudiante 6', lastname: 'Apellido 6', dni:'000007', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user14.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user7.save()
	# user7.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user7.save()

	# user8 = User.new(email: 'est7@cruzcarrillo.com', name: 'Estudiante 7', lastname: 'Apellido 7', dni:'000008', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user13.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user8.save()
	# user8.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user8.save()

	# user9 = User.new(email: 'est8@cruzcarrillo.com', name: 'Estudiante 8', lastname: 'Apellido 8', dni:'000009', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user13.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user9.save()
	# user9.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user9.save()

	# user10 = User.new(email: 'est9@cruzcarrillo.com', name: 'Estudiante 9', lastname: 'Apellido 9', dni:'0000010', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user12.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user10.save()
	# user10.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user10.save()

	# user11 = User.new(email: 'est10@cruzcarrillo.com', name: 'Estudiante 10', lastname: 'Apellido 10', dni:'0000011', birthdate: '2004-06-07', gender:'Femenino', address:'Direccion', status:'Activo', cellphone:'04158524178', representing_id_1: user12.id, password: PASSWORD2, password_confirmation: PASSWORD2)
	# user11.save()
	# user11.role_ids = [Role.where(name: "Estudiante").take!.id]
	# user11.save()

	puts "Users Created"

	Thought.create!(thought:"Un niño puede enseñar tres cosas a un adulto: a ponerse contento sin motivo, a estar siempre ocupado con algo y a saber exigir con todas sus fuerzas aquello que desea", author: "Paulo Coelho")
	Thought.create!(thought:"Nunca consideres el estudio como una obligación, sino como una oportunidad para penetrar en el bello y maravilloso mundo del saber", author: "Albert Einstein")
	Thought.create!(thought:"Aquellos que educan bien a los niños merecen recibir más honores que sus propios padres, porque aquellos sólo les dieron vida, éstos el arte de vivir bien", author: "Aristóteles")
	Thought.create!(thought:"La educación es el arma más poderosa que puedes usar para cambiar el mundo", author: "Nelson Mandela")
	Thought.create!(thought:"El que abre la puerta de una escuela, cierra una prisión", author: "Víctor Hugo")

	puts "Seeding Complete"

end
