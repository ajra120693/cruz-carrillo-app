Rails.application.routes.draw do

  # Root page
  root 'home#index'

  # Foros y comentarios
  # resources :sections do
  #   resources :forums do
  #     resources :coments
  #   end
  # end

  # Foros 
  resources :forums do
    resources :comments
  end

  # Ads (anuncios)
  resources :ads

  # Archivos y calificaciones
  resources :archives

  resources :alumnos
  resources :personals
   
  get '/return_tought' => 'home#return_tought', :as => :return_tought

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions:      'users/sessions'
  }

  devise_scope :user do
    get 'access',        to: 'users/registrations#access'
    put 'access_update', to: 'users/registrations#access_update'
  end

  namespace :admin do
    resources :news, only:[:index, :new, :create, :edit, :update, :destroy]
    resources :subjects
    resources :users, only:[:index, :new, :create, :edit, :update, :destroy]
    resources :sections
    resources :thoughts, only:[:index, :new, :create, :edit, :update, :destroy]
    resources :emails, only:[:index, :destroy]
    get "students_users_with_not_section" => "sections#students_users_with_not_section", :as => :students_users_with_not_section
    get "professors_with_subjects" => "sections#professors_with_subjects", :as => :professors_with_subjects
    get "section_professor_consult/:id" => "sections#section_professor_consult", :as => :section_professor_consult
    patch  "reset_password/:id" => "users#reset_password", :as => :reset_password
    get "send_email_representing" => "users#send_email_representing", :as => :send_email_representing
    get "send_mm_email" => "users#send_mm_email", :as => :send_mm_email
    get "send_ms_email" => "users#send_ms_email", :as => :send_ms_email
    get "check_mode" => "admin#check_mode", :as => :check_mode
    get "update_mode" => "admin#update_mode", :as => :update_mode
  end

  
  # devise_for :users
  # get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  match '*path' => 'application#error_redirect', via: :all
end
