require 'test_helper'

class Admin::AdsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_ad = admin_ads(:one)
  end

  test "should get index" do
    get admin_ads_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_ad_url
    assert_response :success
  end

  test "should create admin_ad" do
    assert_difference('Admin::Ad.count') do
      post admin_ads_url, params: { admin_ad: { date_end: @admin_ad.date_end, date_init: @admin_ad.date_init, description: @admin_ad.description, name: @admin_ad.name, user_id: @admin_ad.user_id } }
    end

    assert_redirected_to admin_ad_url(Admin::Ad.last)
  end

  test "should show admin_ad" do
    get admin_ad_url(@admin_ad)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_ad_url(@admin_ad)
    assert_response :success
  end

  test "should update admin_ad" do
    patch admin_ad_url(@admin_ad), params: { admin_ad: { date_end: @admin_ad.date_end, date_init: @admin_ad.date_init, description: @admin_ad.description, name: @admin_ad.name, user_id: @admin_ad.user_id } }
    assert_redirected_to admin_ad_url(@admin_ad)
  end

  test "should destroy admin_ad" do
    assert_difference('Admin::Ad.count', -1) do
      delete admin_ad_url(@admin_ad)
    end

    assert_redirected_to admin_ads_url
  end
end
