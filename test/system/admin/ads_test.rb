require "application_system_test_case"

class Admin::AdsTest < ApplicationSystemTestCase
  setup do
    @admin_ad = admin_ads(:one)
  end

  test "visiting the index" do
    visit admin_ads_url
    assert_selector "h1", text: "Admin/Ads"
  end

  test "creating a Ad" do
    visit admin_ads_url
    click_on "New Admin/Ad"

    fill_in "Date end", with: @admin_ad.date_end
    fill_in "Date init", with: @admin_ad.date_init
    fill_in "Description", with: @admin_ad.description
    fill_in "Name", with: @admin_ad.name
    fill_in "User", with: @admin_ad.user_id
    click_on "Create Ad"

    assert_text "Ad was successfully created"
    click_on "Back"
  end

  test "updating a Ad" do
    visit admin_ads_url
    click_on "Edit", match: :first

    fill_in "Date end", with: @admin_ad.date_end
    fill_in "Date init", with: @admin_ad.date_init
    fill_in "Description", with: @admin_ad.description
    fill_in "Name", with: @admin_ad.name
    fill_in "User", with: @admin_ad.user_id
    click_on "Update Ad"

    assert_text "Ad was successfully updated"
    click_on "Back"
  end

  test "destroying a Ad" do
    visit admin_ads_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ad was successfully destroyed"
  end
end
