# Preview all emails at http://localhost:3000/rails/mailers/send_message_to_user_mailer
class SendMessageToUserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/send_message_to_user_mailer/email
  def email
    SendMessageToUserMailer.email
  end

end
